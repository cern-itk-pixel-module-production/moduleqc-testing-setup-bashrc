#!/bin/bash
import os
from subprocess import Popen, PIPE, CalledProcessError, run
import time
from datetime import datetime
import argparse
import shutil
import sys
import json
sys.path.append("../sw-interlock-and-monitoring/scripts/")
import watchdog as wd

# TODO:
# - Add list of scans that should be done for each stage (add flag so you can easily run the scans for that stage)
# - Flags for run mode interventions: none (keeps running even if last scan failed), ask (wait for user input), stop (exit())
# - Possibility to use your own config (argument)
# - Add DQ mode, no HV on
# - Update tuning

max_LV_voltage = "3.5"
max_LV_current = {1: "1.47", 2: "2.94", 3: "4.41", 4: "5.88"}

mqt_scans = {
    "adc_calibration" : {"name" : "ADC-CALIBRATION",
                         "dir_name" : "ADC_CALIBRATION",
                         "update_config" : True,
                         "start_with_eyediagram" : True, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "analog_readback" : {"name" : "ANALOG-READBACK",
                         "dir_name" : "ANALOG_READBACK",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "sldo" : {"name" : "SLDO",
                         "dir_name" : "SLDO",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "iv" : {"name" : "IV-MEASURE",
                         "dir_name" : "IV_MEASURE",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : False, 
                         "HV_on" : False, 
                         "LP_mode" : False},
    "lp_mode" : {"name" : "LP-MODE",
                         "dir_name" : "LP_MODE",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : True},
    "vcal_calibration" : {"name" : "VCAL-CALIBRATION",
                         "dir_name" : "VCAL_CALIBRATION",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "injection_capacitance" : {"name" : "INJECTION-CAPACITANCE",
                         "dir_name" : "INJECTION_CAPACITANCE",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "over_voltage" : {"name" : "OVERVOLTAGE-PROTECTION",
                         "dir_name" : "OVERVOLTAGE_PROTECTION",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : True},
    "undershunt" : {"name" : "UNDERSHUNT-PROTECTION",
                         "dir_name" : "UNDERSHUNT_PROTECTION",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : True},
    "data_transmission" : {"name" : "DATA-TRANSMISSION",
                         "dir_name" : "DATA_TRANSMISSION",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "long_term_stability" : {"name" : "LONG-TERM-STABILITY-DCS",
                         "dir_name" : "LONG_TERM_STABILITY_DCS",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False}
                         
}

yarr_scans = {
    "digital_scan" : {"name" : "digital_scan",
                      "config" : "std_digitalscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "digital_scan_m1" : {"name" : "digital_scan",
                      "config" : "std_digitalscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-m"],
                      "additional_args" : ["1"]},
    "analog_scan" : {"name" : "analog_scan",
                      "config" : "std_analogscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "tune_globalthreshold_2000" : {"name" : "tune_global_threshold_2000",
                      "config" : "std_tune_globalthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["2000"]},
    "tune_pixelthreshold_2000" : {"name" : "tune_pixel_threshold_2000",
                      "config" : "std_tune_pixelthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["2000"]},
    "retune_globalthreshold_1500" : {"name" : "retune_global_threshold_2000",
                      "config" : "std_retune_globalthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["1500"]},
    "retune_pixelthreshold_1500" : {"name" : "retune_pixel_threshold_2000",
                      "config" : "std_retune_pixelthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["1500"]},
    "threshold_scan_hr" : {"name" : "threshold_scan_hr",
                      "config" : "std_thresholdscan_hr.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "threshold_scan_hd" : {"name" : "threshold_scan_hd",
                      "config" : "std_thresholdscan_hd.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "tot_scan_6000" : {"name" : "tot_scan",
                      "config" : "std_totscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["6000"]},
    "noise_scan" : {"name" : "noise_scan",
                      "config" : "std_noisescan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "disc_bump_scan" : {"name" : "disc_bump_scan",
                      "config" : "std_discbumpscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "merged_bump_scan" : {"name" : "merged_bump_scan",
                      "config" : "std_mergedbumpscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["2000"]},
    "threshold_retune_zero_bias" : {"name" : "threshold_returne_zero_bias",
                      "config" : "std_retune_pixelthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : False,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["1500"]},
    "threshold_zero_bias" : {"name" : "threshold_zero_bias",
                      "config" : "std_thresholdscan_zerobias.json",
                      "LV_on" : True, 
                      "HV_on" : False,
                      "additional_tags" : [],
                      "additional_args" : []},
    "source_scan" : {"name" : "source_scan",
                      "config" : "selftrigger_source.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["xray"],
                      "additional_args" : []}
}


mqt_scans_list = ["iv", "adc_calibration", "analog_readback", "sldo", "vcal_calibration", "injection_capacitance", "lp_mode", "over_voltage", "undershunt", "data_transmission"]
mqt_scans_list_no_iv = ["adc_calibration", "analog_readback", "sldo", "vcal_calibration", "injection_capacitance", "lp_mode", "over_voltage", "undershunt", "data_transmission"]


mht_scans = ["digital_scan", "analog_scan", "threshold_scan_hr", "tot_scan_6000"]
pfa_scans = ["digital_scan_m1", "analog_scan", "threshold_scan_hd", "noise_scan", "disc_bump_scan","merged_bump_scan", "threshold_retune_zero_bias",  "threshold_zero_bias",  "source_scan" ]
tuning_scans = ["tune_globalthreshold_2000", "threshold_scan_hr", "tot_scan_6000", "tune_globalthreshold_2000", "tune_pixelthreshold_2000", "retune_globalthreshold_1500", "retune_pixelthreshold_1500", "threshold_scan_hd", "tot_scan_6000"]

class FullQC():
    def __init__(self, module_name, verbose = True, full_verbose = False):
        pass
        self.module_name = module_name
        self.base_path = "/home/itkpix/ModuleQC"

        self.verbose = verbose
        self.full_verbose = full_verbose

        self.interrupt_mode = True

        return
    
    def set_variables(self):
        self.set_asn()
        self.mqt_scans_list = mqt_scans_list
        self.mht_scans = mht_scans
        self.tuning_scans = tuning_scans
        self.pfa_scans = pfa_scans

        self.time_measurement_file = "XXX/[module_name]_time_measurements.txt"
        self.log_file = "XXX/[module_name]_log.txt"
        
        self.yarr_path = os.path.join(self.base_path, "Yarr")
        self.qc_tools_path = os.path.join(self.base_path, "module-qc-tools")
        self.module_dir_path = f"{self.base_path}/module_configs/config-{self.module_name}/{self.module_asn}"

        self.merged_vmux_config = "/home/itkpix/ModuleQC/module-qc-tools/venv/lib/python3.9/site-packages/module_qc_tools/data/configs/merged_vmux_electricalQC.json"

        self.localdb_host="pcatlidmod01"
        self.localdb_port="80"
        self.controller="configs/controller/specCfg-rd53b-16x1.json" #TODO full path 

        self.connectivity=f"{self.base_path}/module_configs/config-{self.module_name}/{self.module_asn}/{self.module_asn}_L2_warm.json"
        self.LPconnectivity=f"{self.base_path}/module_configs/config-{self.module_name}/{self.module_asn}/{self.module_asn}_L2_LP.json"
        
        self.set_operational_voltage()
        return
    
    def set_power_supply_status(self, LV_on, HV_on):
        print(f"Setting voltage\nLV on: {LV_on}\nHV on: {HV_on}")
        output, status = self.run_subprocess_non_verbose(["python3", "ramp_psu.py", "-s"])
        output = output.split("\n")
        HV_status = output[0].split(" ")[-1]
        LV_status = output[1].split(" ")[-1]

        if (HV_status == "on") and not (HV_on):
            output, status = self.run_subprocess(["python3", "ramp_psu.py", "-d", "HV"])
        elif (HV_status == "off") and HV_on:
            output, status = self.run_subprocess(["python3", "ramp_psu.py", "-e", "HV"])
            print("Ramping up HV")
            output, status = self.run_subprocess(["python3", "ramp_psu.py", "-rampHV", str(self.HV_voltage)], wait = True)

        if (LV_status == "on") and not (LV_on):
            output, status = self.run_subprocess(["python3", "ramp_psu.py", "-d", "LV"])
        elif (LV_status == "off") and LV_on:
            output, status = self.run_subprocess(["python3", "ramp_psu.py", "-setI", max_LV_current[self.FEs], max_LV_voltage])
            output, status = self.run_subprocess(["python3", "ramp_psu.py", "-e", "LV"])

        return
    
    def run_subprocess(self, cmd, wait = False):
        if self.full_verbose:
            return self.run_subprocess_verbose(cmd, wait=wait)
        else:
            return self.run_subprocess_non_verbose(cmd, wait=wait)
 
    def run_subprocess_non_verbose(self, cmd, store_output = False, wait = False):
        p = run(cmd, stdout=PIPE, encoding = "utf-8")
        return p.stdout, p.returncode

    def run_subprocess_verbose(self, cmd, store_output = False, wait = False):
        p = run(cmd, encoding = "utf-8")
        return p.stdout, p.returncode
    
    def move_to_dir(self, move_dir):
        os.chdir(move_dir)
        return
    
    def run_test(self):
        cmd = ["python3", "test.py"]
        output, status = self.run_subprocess(cmd)
        print("""\n\nPRINTING PUTPUT \n""")
        print(output)
        print(status)


    def run_eyeDiagram(self):
        if not self.check_watchdog():
            raise Exception("Watchdog is not running! Please start watchdog before running an eyeDiagram")
        # TODO: Check LV and HV
        if self.verbose:
            print("Running eyeDiagram")
        self.set_power_supply_status(True, True)

        self.move_to_dir(self.yarr_path)

        cmd = ["./bin/eyeDiagram", "-r", self.controller, "-c", self.connectivity] #./bin/eyeDiagram -r $controller -c $connectivity
        
        if self.analyseED:
           output, status = self.run_subprocess_non_verbose(cmd)
        else:
           output, status = self.run_subprocess_non_verbose(cmd)

        if output!=None:
           print(output)
           results = self.process_eyeDiagram(output)
           if results == []:
              raise Exception("Not able to process eyeDiagram, most likely due to failed communication")
           elif sum(results)<self.FEs:
              print(results)
              raise Exception("Communication not established with all FEs!")
	   

        self.move_to_dir(self.qc_tools_path)

        #self.process_eyeDiagram(output)

        return
    
    def process_eyeDiagram(self, output):
       userows = 20
       output = output.split("\n")
       output = output[-userows:]
       use_channels = ["0","1","2","3"]
       results = []
       for channel in use_channels:
          for row in output:
             if f"for lane {channel}" in row:
                if "No good delay setting" in row:
                   results.append(False)
                else:
                   results.append(True)
                break
       return results
    
    def run_mqt_measurement(self, scan):
        if self.verbose:
            print(f"Running {scan}")
        self.set_power_supply_status(mqt_scans[scan]["LV_on"], mqt_scans[scan]["HV_on"])
        
        scan_name = mqt_scans[scan]["name"]
        if mqt_scans[scan]["LP_mode"] == True:
            connectivity = self.LPconnectivity
        else:
            connectivity = self.connectivity


        if os.path.exists(os.path.join(self.module_dir_path, "tmp")):                                                                                        
            print("Removing tmp file")
            shutil.rmtree(os.path.join(self.module_dir_path, "tmp"))  

        cmd = [f"measurement-{scan_name}", "-c", self.merged_vmux_config, "-m", connectivity, "-o", self.module_dir_path]

        output, status = self.run_subprocess(cmd)

        if scan == "iv":
            output, status = self.run_subprocess(["python3", "ramp_psu.py", "-d", "HV"])
        return
    
    def run_analysis_and_ldb_upload(self, scan):
        if self.verbose:
            print(f"Running analysis after {scan}")

	# Find last measurement
        scan_dir_name = mqt_scans[scan]["dir_name"]
        measurements = []
        for path in os.listdir(os.path.join(self.module_dir_path, "Measurements", scan_dir_name)):
            measurements.append(os.path.join(self.module_dir_path, "Measurements", scan_dir_name, path))
        self.measurement_result_dir = max(measurements, key=os.path.getctime)
        print(f"Measurement dir: {self.measurement_result_dir}")

        # Make analysis dir and file name
        if not os.path.exists(os.path.join(self.module_dir_path, "Analysis", scan_dir_name)):
            os.mkdir(os.path.join(self.module_dir_path, "Analysis", scan_dir_name))

        time_stamp = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        self.analysis_results_dir = os.path.join(self.module_dir_path, "Analysis", scan_dir_name, time_stamp)
        os.mkdir(self.analysis_results_dir)
        self.analysis_results_file = str(os.path.join(self.analysis_results_dir, str(self.module_asn) + ".json"))

        cmd = ["module-qc-tools-upload", "--path", self.measurement_result_dir, "--host", self.localdb_host, "--port", self.localdb_port, "--out", self.analysis_results_file]

        output, status = self.run_subprocess(cmd)
        return
    
    def update_chip_config(self, scan):
        if self.verbose:
            print(f"Updating chip config after {scan}")
        
        scan_name = mqt_scans[scan]["name"]
        if mqt_scans[scan]["LP_mode"] == True:
            qctype = "L2_LP"
        else:
            qctype = "L2_warm"

        cmd = ["analysis-update-chip-config", "-i", self.analysis_results_dir, "-c", self.module_dir_path, "-t", qctype, "--override"]

        print(" ".join(cmd))
        output, status = self.run_subprocess(cmd)
        print(output)
        return

    def run_mqt_scan(self, scan):
        if not self.check_watchdog():
            raise Exception("Watchdog is not running! Please start watchdog before running a scan")
        if mqt_scans[scan]["start_with_eyediagram"]:
            self.run_eyeDiagram()

        self.run_mqt_measurement(scan)

        self.run_analysis_and_ldb_upload(scan)

        if mqt_scans[scan]["update_config"]:
            self.update_chip_config(scan)

        return

    def run_yarr_scan(self, scan, tag):
        if not self.check_watchdog():
            raise Exception("Watchdog is not running! Please start watchdog before running a scan")
        if self.verbose:
            print(f"Running {scan}")		 

        scan_config = yarr_scans[scan]["config"]

        if self.yarr_tag != None:
            tag = self.yarr_tag
	
        cmd = ["./bin/scanConsole", "-r", self.controller, "-c", self.connectivity, "-s", f"configs/scans/rd53b/{scan_config}", "-o", os.path.join(self.module_dir_path, "data"), "-W", tag]

	
        if yarr_scans[scan]["additional_tags"] != []:
            if "xray" in  yarr_scans[scan]["additional_tags"]:
                run = input("Starting sourcescan. Enter y when the source is on to start source scan, or n to cancel the source scan.")
                if run != "y":
                    print("Sourcescan canceled")
                    return
            else:
                for i in range(len(yarr_scans[scan]["additional_tags"])):
                    cmd.append(yarr_scans[scan]["additional_tags"][i])
                    cmd.append(yarr_scans[scan]["additional_args"][i])
		    
        self.set_power_supply_status(yarr_scans[scan]["LV_on"], yarr_scans[scan]["HV_on"])
       

        self.move_to_dir(self.yarr_path)
        output, status = self.run_subprocess(cmd)

        if scan == "threshold_zero_bias":
            self.revert_chip_config()
        self.move_to_dir(self.qc_tools_path)
        return


    def run_mht(self):
        self.run_eyeDiagram()
        for scan in self.mht_scans:
            self.run_yarr_scan(scan, "MHT")
        return

    def run_pfa(self):
        for scan in self.pfa_scans:
            self.run_yarr_scan(scan, "PFA")
        return

    def run_tuning(self):
        for scan in self.tuning_scans:
            self.run_yarr_scan(scan, "TUN")
        return

    def run_mqt(self):
        for scan in self.mqt_scans_list:
            self.run_mqt_scan(scan)
            if scan == "iv":
                self.set_operational_voltage()
        return
    
    def alert_user(self, topic, message):
        return

    def set_asn(self):
        self.module_asn = os.listdir(f"{self.base_path}/module_configs/config-{self.module_name}/")[0]
        return

    def generate_config(self, ASN, wafer = False):
        cmd = ["generateYARRConfig", "-sn", ASN, "-o", f"{self.base_path}/module_configs/config-{self.module_name}/"]

        if wafer == True:
            cmd.append(["-v", "TESTONWAFER"])

        output, status = self.run_subprocess(cmd)
        os.mkdir(f"{self.base_path}/module_configs/config-{self.module_name}/{ASN}/Analysis")

    def check_watchdog(self):
        pid = wd.get_pid_from_file("../sw-interlock-and-monitoring/watchdog.pid")
        return wd.check_if_process_is_running(pid)    


    def set_operational_voltage(self):
        scan_dir_name = mqt_scans["iv"]["dir_name"]
        analyses = []
        for path in os.listdir(os.path.join(self.module_dir_path, "Analysis", scan_dir_name)):
            path = os.path.join(self.module_dir_path, "Analysis", scan_dir_name, path)
            if os.path.exists(os.path.join(path, f"{self.module_asn}.json")):
               analyses.append(os.path.join(path, f"{self.module_asn}.json"))
        if analyses == []:
           self.HV_voltage = -120
           return
        last_iv_analysis = max(analyses, key=os.path.getctime)
        breakdown_observed, breakdown_voltage, passed_QC, max_voltage = self.read_IV_analysis_output(last_iv_analysis)
        self.HV_voltage = self.get_operational_voltage(breakdown_observed, breakdown_voltage, passed_QC, max_voltage)
        print(self.HV_voltage)
        if (int(self.HV_voltage) > -5) or (int(self.HV_voltage) < -120):
           raise Exception(f"Did not succed to find a correct operational voltage. Operational voltage found: {self.HV_voltage}")
           exit()
	
    def get_operational_voltage(self, breakdown_observed, breakdown_voltage, passed_QC, max_voltage):
        default_voltage = -120
        breakdown_observed, breakdown_voltage, passed_QC, max_voltage
        breakdown_voltage = - abs(breakdown_voltage)
        max_voltage = - abs(max_voltage)
        if (passed_QC) and (not breakdown_observed):
            return default_voltage
        elif breakdown_observed:
            if breakdown_voltage < -130:
                return default_voltage
            else:
                return breakdown_voltage + 10
        elif (not passed_QC) and (not breakdown_observed):
            if max_voltage<=-120:
                return default_voltage
            elif max_voltage > -15:
                return -5
            else:
                return max_voltage + 10


    def read_IV_analysis_output(self, filename):
        with open(filename, "r") as openfile:
            file_dict = json.load(openfile)[0]
        passed_QC = file_dict["passed"]
        breakdown_observed = not file_dict["results"]['NO_BREAKDOWN_VOLTAGE_OBSERVED']
        breakdown_voltage = file_dict["results"]['BREAKDOWN_VOLTAGE']
        max_voltage = file_dict["results"]['MAXIMUM_VOLTAGE']
        return breakdown_observed, breakdown_voltage, passed_QC, max_voltage
    
    def revert_chip_config(self, config_dir = False):
        if config_dir == False:
            config_dirs = []
            for path in os.listdir(os.path.join(self.module_dir_path, "data")):
                path = os.path.join(self.module_dir_path, "data", path)
                if "retune_pixelthreshold" in str(path):
                    config_dirs.append(os.path.join(path, f"{self.module_asn}.json"))
            if config_dirs == []:
                raise Exception("Could not revert chip config, did not find a 'retune_pixelthreshold'scan")
            last_retune = max(config_dirs, key=os.path.getctime)
        else:
            last_retune = config_dir

        self.move_to_dir(self.yarr_path)
        cmd = ["./bin/revert-chip-configs", "-c", self.connectivity, "-d", last_retune]
        output, status = self.run_subprocess(cmd)
        return

    
    def main(self):
        self.scan_start = time.time()

        # Source virtual environments 
        ##echo "Sourcing labRemote venv"
        ##source ../labRemote/myenv/bin/activate
        ##echo "Sourcing module-qc-tools venv"
        ##source venv/bin/activate

        print("Running module-qc-tools scans")
        self.run_mqt()

        print("Running minimal health test")
        self.run_mht()

        print("Running tuning")
        self.run_tuning()

        print("Running pixel failure analysis")
        self.run_pfa()

        self.set_power_supply_status(False, False)
        #Finish log file and timing file
        self.scan_done = time.time()
        print("The scans finished in ")
        print((self.scan_done - self.scan_start)/60)
        print("minutes")




if __name__ == "__main__":
   parser = argparse.ArgumentParser()
   parser.add_argument("-m", "--moduleName", help = "Name of module, e.g. CERNPixQ50. The folder with the connectivity files must have this name")
   parser.add_argument("-asn", "--asn", help = "Module ASN. Only needed to generate Yarr config.")
   parser.add_argument("-ed", "--eyeDiagram", help = "Run an eyeDiagram", action = "store_true")
   parser.add_argument("-mqt", "--moduleQCtools", help = f"Run module-qc-tools scan. Follow by name of scans, or run all scan if no value is provided. Possible scans: {', '.join(mqt_scans.keys())}", nargs="*")
   parser.add_argument("-yarr", "--yarr", help = f"Run yarr scan. Follow by name of scans, or run MHT + TUN + PFA if no value is provided. Possible scans: {', '.join(yarr_scans.keys())}", nargs="*")
   parser.add_argument("-mht", "--minimalHealthTest", help = "Run MHT.", action = "store_true")
   parser.add_argument("-tun", "--tuning", help = "Run tuning.", action = "store_true")
   parser.add_argument("-pfa", "--pixelFailureAnalysis", help = "Run PFA.", action = "store_true")
   parser.add_argument("-fullQC", "--fullQC", help = "Run all mqt scans and Yarr scans.", action = "store_true")
   parser.add_argument("-fullQC_no_iv", "--fullQC_no_iv", help = "Run all mqt scans apart from iv, and all Yarr scans.", action = "store_true")
   parser.add_argument("-ana", "--analyseLatest", help = "Analyse the latest mqt scan. List one or more scans to analyse", nargs="+")
   parser.add_argument("-v", "--verbose", help = "If all output from scan should be printed.", action = "store_true")
   parser.add_argument("-pon", "--pOn", help = "If power should be kept on after scans finish. Defualt is to turn off LV and HV", action = "store_true")
   parser.add_argument("-tag", "--yarrTag", help = "Tag to use for Yarr scans.")
   parser.add_argument("-gyc", "--generateYARRConfig", help = "Generate config from PDB", action = "store_true")
   parser.add_argument("-FEs", "--FEs", help = "Number of functioning FEs. If not all FEs are functioning the currents will be adapted.")
   parser.add_argument("-noAnaED", "--noAnaED", help = "Use this if you dont want automatic analysis of eyeDiagram", action = "store_true")
   
   args = parser.parse_args()

   module_name = args.moduleName
   verbose = args.verbose

   full_QC = FullQC(module_name, full_verbose=verbose)

   if args.generateYARRConfig:
      full_QC.generate_config(args.asn)

   full_QC.yarr_tag = args.yarrTag

   full_QC.set_variables()
   
   full_QC.analyseED = True
   if args.noAnaED == True:
      full_QC.analyseED = False

   if args.FEs:
      full_QC.FEs = int(args.FEs)
      if int(args.FEs) == 1:
         full_QC.merged_vmux_config = "/home/itkpix/ModuleQC/module-qc-tools/venv/lib/python3.9/site-packages/module_qc_tools/data/configs/merged_vmux_electricalQC_1FE.json"
      elif int(args.FEs) == 2:
         full_QC.merged_vmux_config = "/home/itkpix/ModuleQC/module-qc-tools/venv/lib/python3.9/site-packages/module_qc_tools/data/configs/merged_vmux_electricalQC_2FEs.json"
      elif int(args.FEs) == 3:
           full_QC.merged_vmux_config = "/home/itkpix/ModuleQC/module-qc-tools/venv/lib/python3.9/site-packages/module_qc_tools/data/configs/merged_vmux_electricalQC_3FEs.json"
   else:
      full_QC.FEs = 4
   
   if args.eyeDiagram == True:
      full_QC.run_eyeDiagram()

   if args.moduleQCtools is not None:
      if args.moduleQCtools != []:
         full_QC.mqt_scans_list = args.moduleQCtools
      full_QC.run_mqt()

   if args.yarr is not None:
      if args.yarr != []:
         for scan in args.yarr:
            full_QC.run_yarr_scan(scan, "Yarr")
      else:
         full_QC.run_mht()
         full_QC.run_tuning()
         full_QC.run_pfa()

   if args.fullQC:
      full_QC.main()

   if args.fullQC_no_iv:
      full_QC.mqt_scans_list = mqt_scans_list_no_iv
      full_QC.main()

   if args.minimalHealthTest:
      full_QC.run_mht()

   if args.tuning:
      full_QC.run_tuning()

   if args.pixelFailureAnalysis:
      full_QC.run_pfa()

   if args.analyseLatest:
      for scan in args.analyseLatest:
          full_QC.run_analysis_and_ldb_upload(scan)
      exit()
	  
   if not args.pOn:
       full_QC.set_power_supply_status(False, False)

