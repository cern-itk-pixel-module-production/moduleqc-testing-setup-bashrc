#!/usr/bin/env pytyhon3

import os
from subprocess import Popen, PIPE, CalledProcessError, run
import time
from datetime import datetime
import argparse
import shutil
import edit_config_file_quadsetup as ecf

# TODO:
# - Add list of scans that should be done for each stage (add flag so you can easily run the scans for that stage)
# - Always delete tmp file before starting
# - Flags for run mode interventions: none (keeps running even if last scan failed), ask (wait for user input), stop (exit())
# - Possibility to use your own config (argument)
# - Add DQ mode, no HV on
# - Update tuning

mqt_scans = {
    "adc_calibration" : {"name" : "ADC-CALIBRATION",
                         "dir_name" : "ADC_CALIBRATION",
                         "update_config" : True,
                         "start_with_eyediagram" : True, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "analog_readback" : {"name" : "ANALOG-READBACK",
                         "dir_name" : "ANALOG_READBACK",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "sldo" : {"name" : "SLDO",
                         "dir_name" : "SLDO",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "iv" : {"name" : "IV-MEASURE",
                         "dir_name" : "IV_MEASURE",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : False, 
                         "LP_mode" : False},
    "lp_mode" : {"name" : "LP-MODE",
                         "dir_name" : "LP_MODE",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : True},
    "vcal_calibration" : {"name" : "VCAL-CALIBRATION",
                         "dir_name" : "VCAL_CALIBRATION",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "injection_capacitance" : {"name" : "INJECTION-CAPACITANCE",
                         "dir_name" : "INJECTION_CAPACITANCE",
                         "update_config" : True,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "over_voltage" : {"name" : "OVERVOLTAGE-PROTECTION",
                         "dir_name" : "OVERVOLTAGE_PROTECTION",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : True},
    "undershunt" : {"name" : "UNDERSHUNT-PROTECTION",
                         "dir_name" : "UNDERSHUNT_PROTECTION",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : True},
    "data_transmission" : {"name" : "DATA-TRANSMISSION",
                         "dir_name" : "DATA_TRANSMISSION",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False},
    "long_term_stability" : {"name" : "LONG-TERM-STABILITY-DCS",
                         "dir_name" : "LONG_TERM_STABILITY_DCS",
                         "update_config" : False,
                         "start_with_eyediagram" : False, 
                         "LV_on" : True, 
                         "HV_on" : True, 
                         "LP_mode" : False}
                         
}

yarr_scans = {
    "digital_scan" : {"name" : "digital_scan",
                      "config" : "std_digitalscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "digital_scan_m1" : {"name" : "digital_scan",
                      "config" : "std_digitalscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-m"],
                      "additional_args" : ["1"]},
    "analog_scan" : {"name" : "analog_scan",
                      "config" : "std_analogscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "tune_globalthreshold_2000" : {"name" : "tune_global_threshold_2000",
                      "config" : "std_tune_globalthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["2000"]},
    "tune_pixelthreshold_2000" : {"name" : "tune_pixel_threshold_2000",
                      "config" : "std_tune_pixelthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["2000"]},
    "retune_globalthreshold_1500" : {"name" : "retune_global_threshold_2000",
                      "config" : "std_retune_globalthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["1500"]},
    "retune_pixelthreshold_1500" : {"name" : "retune_pixel_threshold_2000",
                      "config" : "std_retune_pixelthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["1500"]},
    "threshold_scan_hr" : {"name" : "threshold_scan_hr",
                      "config" : "std_thresholdscan_hr.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "threshold_scan_hd" : {"name" : "threshold_scan_hd",
                      "config" : "std_thresholdscan_hd.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "tot_scan_6000" : {"name" : "tot_scan",
                      "config" : "std_totscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["6000"]},
    "noise_scan" : {"name" : "noise_scan",
                      "config" : "std_noisescan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "disc_bump_scan" : {"name" : "disc_bump_scan",
                      "config" : "std_discbumpscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : [],
                      "additional_args" : []},
    "merged_bump_scan" : {"name" : "merged_bump_scan",
                      "config" : "std_mergedbumpscan.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["2000"]},
    "threshold_retune_zero_bias" : {"name" : "threshold_returne_zero_bias",
                      "config" : "std_retune_pixelthreshold.json",
                      "LV_on" : True, 
                      "HV_on" : False,
                      "additional_tags" : ["-t"],
                      "additional_args" : ["1500"]},
    "threshold_zero_bias" : {"name" : "threshold_zero_bias",
                      "config" : "std_thresholdscan_zerobias.json",
                      "LV_on" : True, 
                      "HV_on" : False,
                      "additional_tags" : [],
                      "additional_args" : []},
    "source_scan" : {"name" : "source_scan",
                      "config" : "selftrigger_source.json",
                      "LV_on" : True, 
                      "HV_on" : True,
                      "additional_tags" : ["xray"],
                      "additional_args" : []}
}


mqt_scans_list = ["iv", "adc_calibration", "analog_readback", "sldo", "vcal_calibration", "injection_capacitance", "lp_mode", "over_voltage", "undershunt", "data_transmission"]

mht_scans = ["digital_scan", "analog_scan", "threshold_scan_hr", "tot_scan_6000"]
pfa_scans = ["digital_scan_m1", "analog_scan", "threshold_scan_hd", "noise_scan", "disc_bump_scan","merged_bump_scan", "threshold_retune_zero_bias",  "threshold_zero_bias"]
tuning_scans = ["tune_globalthreshold_2000", "threshold_scan_hr", "tot_scan_6000", "tune_globalthreshold_2000", "tune_pixelthreshold_2000", "retune_globalthreshold_1500", "retune_pixelthreshold_1500", "threshold_scan_hd", "tot_scan_6000"]

class FullQC():
    def __init__(self, module_name, verbose = True, full_verbose = False):
        pass
        self.module_name = module_name
        self.base_path = "/home/common"

        self.verbose = verbose
        self.full_verbose = full_verbose

        self.interrupt_mode = True
        return
    
    def set_variables(self, jig = 1):
        self.set_asn()
        self.mqt_scans_list = mqt_scans_list
        self.mht_scans = mht_scans
        self.tuning_scans = tuning_scans
        self.pfa_scans = pfa_scans
        self.jig = jig

        self.time_measurement_file = "XXX/[module_name]_time_measurements.txt"
        self.log_file = "XXX/[module_name]_log.txt"
        
        self.yarr_path = os.path.join(self.base_path, "Yarr")
        self.qc_tools_path = os.path.join(self.base_path, "module-qc-tools")
        self.module_dir_path = f"{self.base_path}/Yarr/configs/connectivity/{self.module_name}/{self.module_asn}"

        self.merged_vmux_config = f"/home/common/module-qc-tools/src/module_qc_tools/data/configs/example_merged_vmuxjig{jig}.json"

        #self.localdb_host="pcatlidmod01"
        self.localdb_port="80"
        self.controller="configs/controller/specCfg-rd53b-16x1.json" #TODO full path 

        self.connectivity=f"{self.module_dir_path}/{self.module_asn}_L2_warm.json"
        self.LPconnectivity=f"{self.module_dir_path}/{self.module_asn}_L2_LP.json"

        self.set_tx_rx(self.connectivity)
        self.set_tx_rx(self.LPconnectivity)

        return
    
    def set_power_supply_status(self, LV_on, HV_on):
        print(f"Setting voltage\nLV on: {LV_on}\nHV on: {HV_on}")
        #output, status = self.run_subprocess_non_verbose(["python3", "ramp_psu.py", "-s"])
        #output = output.split("\n")
        #HV_status = output[0].split(" ")[-1]
        #LV_status = output[1].split(" ")[-1]

        if not (HV_on):
            output, status = self.run_subprocess(["python3", "/home/common/module-qc-scripts/scripts/multipowermodule.py", "-c", "/home/common/module-qc-scripts/configs/multidcs_cern_updated.json", "-HV", "-ch", self.jig, "-off"])
        elif HV_on:
            print("Ramping up HV")
            output, status = self.run_subprocess(["python3", "/home/common/module-qc-scripts/scripts/multipowermodule.py", "-c", "/home/common/module-qc-scripts/configs/multidcs_cern_updated.json", "-HV", "-ch", self.jig, "-on", "-v", "-120"])

        if not (LV_on):
            output, status = self.run_subprocess(["python3", "/home/common/module-qc-scripts/scripts/multipowermodule.py", "-c", "/home/common/module-qc-scripts/configs/multidcs_cern_updated.json", "-LV", "-ch", self.jig, "-off"])
        elif LV_on:
            output, status = self.run_subprocess(["python3", "/home/common/module-qc-scripts/scripts/multipowermodule.py", "-c", "/home/common/module-qc-scripts/configs/multidcs_cern_updated.json", "-LV", "-ch", self.jig, "-on"])

        return
    
    def run_subprocess(self, cmd, wait = False):
        if self.full_verbose:
            return self.run_subprocess_verbose(cmd, wait=wait)
        else:
            return self.run_subprocess_non_verbose(cmd, wait=wait)
 
    def run_subprocess_non_verbose(self, cmd, store_output = False, wait = False):
        p = run(cmd, stdout=PIPE, encoding = "utf-8")
        return p.stdout, p.returncode

    def run_subprocess_verbose(self, cmd, store_output = False, wait = False):
        p = run(cmd, encoding = "utf-8")
        return p.stdout, p.returncode
    
    def move_to_dir(self, move_dir):
        os.chdir(move_dir)
        return


    def run_eyeDiagram(self):
        # TODO: Check LV and HV
        if self.verbose:
            print("Running eyeDiagram")
        self.set_power_supply_status(True, True)

        self.move_to_dir(self.yarr_path)

        cmd = ["./bin/eyeDiagram", "-r", self.controller, "-c", self.connectivity] #./bin/eyeDiagram -r $controller -c $connectivity
        
        output, status = self.run_subprocess(cmd)

        self.move_to_dir(self.qc_tools_path)

        self.process_eyeDiagram(output)

        return
    
    def process_eyeDiagram(self, output):
        pass
    
    def run_mqt_measurement(self, scan):
        if self.verbose:
            print(f"Running {scan}")
        self.set_power_supply_status(mqt_scans[scan]["LV_on"], mqt_scans[scan]["HV_on"])
        
        scan_name = mqt_scans[scan]["name"]
        if mqt_scans[scan]["LP_mode"] == True:
            connectivity = self.LPconnectivity
        else:
            connectivity = self.connectivity

        if os.path.exists(os.path.join(self.module_dir_path, "tmp")):
            shutil.rmtree(os.path.join(self.module_dir_path, "tmp"))

        cmd = [f"measurement-{scan_name}", "-c", self.merged_vmux_config, "-m", connectivity, "-o", self.module_dir_path]

        output, status = self.run_subprocess(cmd)

        return
    
    def run_analysis_and_ldb_upload(self, scan):
        if self.verbose:
            print(f"Running analysis after {scan}")

	# Find last measurement
        scan_dir_name = mqt_scans[scan]["dir_name"]
        measurements = []
        for path in os.listdir(os.path.join(self.module_dir_path, "Measurements", scan_dir_name)):
            measurements.append(os.path.join(self.module_dir_path, "Measurements", scan_dir_name, path))
        self.measurement_result_dir = max(measurements, key=os.path.getctime)
        print(f"Measurement dir: {self.measurement_result_dir}")

        # Make analysis dir and file name
        if not os.path.exists(os.path.join(self.module_dir_path, "Analysis", scan_dir_name)):
            os.makedirs(os.path.join(self.module_dir_path, "Analysis", scan_dir_name))

        time_stamp = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        self.analysis_results_dir = os.path.join(self.module_dir_path, "Analysis", scan_dir_name, time_stamp)
        os.mkdir(self.analysis_results_dir)
        self.analysis_results_file = str(os.path.join(self.analysis_results_dir, str(self.module_asn) + ".json"))

        cmd = ["module-qc-tools-upload", "--path", self.measurement_result_dir, "--host", self.localdb_host, "--port", self.localdb_port, "--out", self.analysis_results_file]

        output, status = self.run_subprocess(cmd)
        return
    
    def update_chip_config(self, scan):
        if self.verbose:
            print(f"Updating chip config after {scan}")
        
        scan_name = mqt_scans[scan]["name"]
        if mqt_scans[scan]["LP_mode"] == True:
            qctype = "L2_LP"
        else:
            qctype = "L2_warm"

        cmd = ["analysis-update-chip-config", "-i", self.analysis_results_dir, "-c", self.module_dir_path, "-t", qctype, "--override"]

        print(" ".join(cmd))
        output, status = self.run_subprocess(cmd)
        print(output)
        return

    def run_mqt_scan(self, scan):
        if mqt_scans[scan]["start_with_eyediagram"]:
            self.run_eyeDiagram()

        self.run_mqt_measurement(scan)

        self.run_analysis_and_ldb_upload(scan)

        if mqt_scans[scan]["update_config"]:
            self.update_chip_config(scan)

        return

    def run_yarr_scan(self, scan, tag):
        if self.verbose:
            print(f"Running {scan}")		 

        scan_config = yarr_scans[scan]["config"]
	
        if self.database_json == None:
            cmd = ["./bin/scanConsole", "-r", self.controller, "-c", self.connectivity, "-s", f"configs/scans/rd53b/{scan_config}", "-o", os.path.join(self.module_dir_path, "data")]

        else:
            cmd = ["./bin/scanConsole", "-r", self.controller, "-c", self.connectivity, "-s", f"configs/scans/rd53b/{scan_config}", "-o", os.path.join(self.module_dir_path, "data"), "-W", tag, "-d", self.database_json]

	
        if yarr_scans[scan]["additional_tags"] != []:
            if "xray" in  yarr_scans[scan]["additional_tags"]:
                run = input("Starting sourcescan. Enter y when the source is on to start source scan, or n to cancel the source scan.")
                if run != "y":
                    print("Sourcescan canceled")
                    return
            else:
                for i in range(len(yarr_scans[scan]["additional_tags"])):
                    cmd.append(yarr_scans[scan]["additional_tags"][i])
                    cmd.append(yarr_scans[scan]["additional_args"][i])
		    
        self.set_power_supply_status(yarr_scans[scan]["LV_on"], yarr_scans[scan]["HV_on"])
       

        self.move_to_dir(self.yarr_path)
        output, status = self.run_subprocess(cmd)
        self.move_to_dir(self.qc_tools_path)
        return


    def run_mht(self):
        self.run_eyeDiagram()
        for scan in self.mht_scans:
            self.run_yarr_scan(scan, "MHT")
        return

    def run_pfa(self):
        for scan in self.pfa_scans:
            self.run_yarr_scan(scan, "PFA")
        return

    def run_tuning(self):
        for scan in self.tuning_scans:
            self.run_yarr_scan(scan, "TUN")
        return

    def run_mqt(self):
        for scan in self.mqt_scans_list:
            self.run_mqt_scan(scan)
        return
    
    def alert_user(self, topic, message):
        return

    def set_asn(self):
        self.module_asn = os.listdir(f"{self.base_path}/Yarr/configs/connectivity/{self.module_name}/")[0]
        return

    def generate_config(self, ASN, wafer = False):
        cmd = ["generateYARRConfig", "-sn", ASN, "-o", f"{self.base_path}/Yarr/configs/connectivity/{self.module_name}/"]

        if wafer == True:
            cmd.append(["-v", "TESTONWAFER"])

        output, status = self.run_subprocess(cmd)
        os.mkdir(f"{self.base_path}/Yarr/configs/connectivity/{self.module_name}/{ASN}/Analysis")

    def set_tx_rx(self, connectivity_filename):
        connectivity_dict = ecf.read_json(connectivity_filename)
        connectivity_dict = ecf.replace_tx_rx(connectivity_dict, self.jig)
        ecf.overwrite_connectivity(connectivity_dict, connectivity_filename)
        return
        
    
    def main(self):
        self.scan_start = time.time()

        # Source virtual environments 
        ##echo "Sourcing labRemote venv"
        ##source ../labRemote/myenv/bin/activate
        ##echo "Sourcing module-qc-tools venv"
        ##source venv/bin/activate

        print("Running module-qc-tools scans")
        self.run_mqt()

        print("Running minimal health test")
        self.run_mht()

        print("Running tuning")
        self.run_tuning()

        print("Running pixel failure analysis")
        self.run_pfa()

        self.set_power_supply_status(False, False)
        #Finish log file and timing file
        self.scan_done = time.time()
        print("The scans finished in ")
        print((self.scan_done - self.scan_start)/60)
        print("minutes")




if __name__ == "__main__":
   parser = argparse.ArgumentParser()
   parser.add_argument("-m", "--moduleName", help = "Name of module, e.g. CERNPixQ50. The folder with the connectivity files must have this name", required = True)
   parser.add_argument("-asn", "--asn", help = "Module ASN. Only needed to generate Yarr config.")
   parser.add_argument("-ed", "--eyeDiagram", help = "Run an eyeDiagram", action = "store_true")
   parser.add_argument("-mqt", "--moduleQCtools", help = "Run module-qc-tools scan. Follow by name of scans, or run all scan if no value is provided", nargs="*")
   parser.add_argument("-yarr", "--yarr", help = "Run yarr scan. Follow by name of scans, or run MHT + TUN + PFA if no value is provided", nargs="*")
   parser.add_argument("-mht", "--minimalHealthTest", help = "Run MHT.", action = "store_true")
   parser.add_argument("-tun", "--tuning", help = "Run tuning.", action = "store_true")
   parser.add_argument("-pfa", "--pixelFailureAnalysis", help = "Run PFA.", action = "store_true")
   parser.add_argument("-fullQC", "--fullQC", help = "Run all mqt scans and Yarr scans.", action = "store_true")
   parser.add_argument("-ana", "--analyseLatest", help = "Analyse the latest mqt scan. List one or more scans to analyse", nargs="+")
   parser.add_argument("-v", "--verbose", help = "If all output from scan should be printed.", action = "store_true")
   parser.add_argument("-pon", "--pOn", help = "If power should be kept on after scans finish. Defualt is to turn off LV and HV", action = "store_true")
   parser.add_argument("-tag", "--yarrTag", help = "Tag to use for Yarr scans.")
   parser.add_argument("-gyc", "--generateYARRConfig", help = "Generate config from PDB", action = "store_true")
   parser.add_argument("-j", "--jig", help = "Jig the module is installed in.", required = True)
   parser.add_argument("-ldb", "--ldb", help = "Which local db instance to upload scans to. Options are 'loaded' and 'non-loaded'")
   
   args = parser.parse_args()

   module_name = args.moduleName
   verbose = args.verbose

   full_QC = FullQC(module_name, full_verbose=verbose)
 
   if args.generateYARRConfig:
      full_QC.generate_config(args.asn)
      exit()

   full_QC.set_variables(jig = args.jig)

   if args.ldb == "loaded":
      full_QC.localdb_host="pcatlidmod09"
      full_QC.database_json = "/home/itkpix/.yarr/localdb/pcatlidmod04_database.json"
   elif args.ldb == "non-loaded":
      full_QC.localdb_host="pcatlidmod01"
      full_QC.database_json = "/home/itkpix/.yarr/localdb/pcatlidmod05_database.json"
   elif args.ldb == "None":
      full_QC.localdb_host="pcatlidmod09"
      full_QC.database_json = None
   else:
       raise Exception("Please specify which local db instance you wish to use with the -ldb argument. Options are 'loaded' and 'non-loaded'")

   full_QC.yarr_tag = args.yarrTag

  
   print(full_QC.merged_vmux_config)
   print(full_QC.module_dir_path)
   print(full_QC.connectivity)

   if args.eyeDiagram == True:
      full_QC.run_eyeDiagram()

   if args.moduleQCtools is not None:
      if args.moduleQCtools != []:
         full_QC.mqt_scans_list = args.moduleQCtools
      full_QC.run_mqt()

   if args.yarr is not None:
      if args.yarr != []:
         for scan in args.yarr:
            full_QC.run_yarr_scan(scan, "TEST")
      else:
         full_QC.run_mht()
         full_QC.run_tuning()
         full_QC.run_pfa()

   if args.fullQC:
      full_QC.main()

   if args.minimalHealthTest:
      full_QC.run_mht()

   if args.tuning:
      full_QC.run_tuning()

   if args.pixelFailureAnalysis:
      full_QC.run_pfa()

   if args.analyseLatest:
      for scan in args.analyseLatest:
          full_QC.run_analysis_and_ldb_upload(scan)


   #full_QC.run_mqt_measurement("iv")
   #full_QC.run_analysis_and_ldb_upload("iv")
   #full_QC.run_tuning()
   #full_QC.run_pfa()
   #full_QC.run_mqt_scan("over_voltage")
   if not args.pOn:
        full_QC.set_power_supply_status(False, False)
        


