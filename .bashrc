# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
        for rc in ~/.bashrc.d/*; do
                if [ -f "$rc" ]; then
                        . "$rc"
                fi
        done
fi

# User specific aliases and functions
export PYTHONPATH=/home/itkpix/labRemote/build/lib:$PYTHONPATH
source /home/Xilinx/2022.1/settings64.sh

#ITk Production Database
export INSTITUTION="CERN_PIXEL_MODULES"
export ITKDB_ACCESS_CODE1="kyhmac-zamdYg-6qydmu"
export ITKDB_ACCESS_CODE2="kyhmac-zamdYg-6qydm2"

unset rc
